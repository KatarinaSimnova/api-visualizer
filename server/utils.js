const isEntityNotFoundWhileUpdating = result => result && result[0] === 0;

module.exports = { isEntityNotFoundWhileUpdating };
