import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { logout } from './../client';
import { view } from 'react-easy-state';
import dataStore from '../stores/data-store';

class Header extends Component {
  render() {
    return (
      <header>
        <Link to="/">
          <h1 className="app-logo">API Visualizer</h1>
        </Link>
        {dataStore.user ? (
          <div className="navbar-item has-dropdown is-hoverable main-menu button">
            <a className="navbar-link">{dataStore.user.username}</a>
            <div className="navbar-dropdown is-boxed">
              <Link to="/mydata" className="navbar-item">
                My data
              </Link>
              <Link className="navbar-item" onClick={logout} to="/">
                Logout
              </Link>
            </div>
          </div>
        ) : (
          <Link to="/login" className="button">
            Login
          </Link>
        )}
      </header>
    );
  }
}

export default view(Header);
