import React, { Component } from 'react';
import { view } from 'react-easy-state';
import Visualization from './Visualization';
import dataStore from 'stores/data-store';
import { getJsons, updateJson, deleteJson } from '../client';
import update from 'immutability-helper';

class MyData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
  }
  componentDidMount() {
    getJsons();
  }
  render() {
    console.log(this.state.data);
    return (
      <div className="mainWindow">
        <div className="left-panel">
          <h1 className="is-size-5 has-text-centered">My data</h1>
          {dataStore.jsons.map((j, i) => (
            <div key={i} className="row">
              <a className="json-item" onClick={() => this.setState({ data: j })}>
                {new Date(j.createdAt).toLocaleString('de')}
              </a>
              <a
                onClick={() => {
                  deleteJson(j.id);
                  this.setState({ data: null });
                }}
                className="is-pulled-right borderless-button"
              >
                <span className="icon is-small icon-centered">
                  <i className="fas fa-times" />
                </span>
              </a>
            </div>
          ))}
        </div>
        <div className="main-content">
          {this.state.data && (
            <Visualization
              data={this.state.data.jsonData}
              isEditable
              onChange={v => this.setState(update(this.state, { data: { jsonData: { $set: v } } }))}
              onUpdate={() => updateJson(this.state.data.id, this.state.data)}
            />
          )}
        </div>
      </div>
    );
  }
}

export default view(MyData);
