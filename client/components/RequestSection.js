import React, { Component } from 'react';
import DropDown from './reusable/Dropdown';
import update from 'immutability-helper';
import { saveRequest, sendRequest, getRequests, isLoggedIn } from '../client';
import { view } from 'react-easy-state';
import dataStore from 'stores/data-store';
import uiStore from 'stores/ui-store';

class RequestSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isParamSectionVisible: true,
      errors: {},
      isLoading: false,
      submitError: null
    };
    dataStore.result = {
      data: [
        'abo',
        ['ab', 'abf'],
        {
          postId: [1, 2, 3],
          id: {
            a: ['k', 'l', 'i'],
            b: 2
          },
          name: 'id labore ex et quam laborum',
          email: 'Eliseo@gardner.biz',
          body:
            'laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium'
        },
        {
          postId: 1,
          id: 2,
          name: 'quo vero reiciendis velit similique earum',
          email: 'Jayne_Kuhic@sydney.com',
          body:
            'est natus enim nihil est dolore omnis voluptatem numquam\net omnis occaecati quod ullam at\nvoluptatem error expedita pariatur\nnihil sint nostrum voluptatem reiciendis et'
        },
        {
          postId: 1,
          id: 3,
          name: 'odio adipisci rerum aut animi',
          email: 'Nikita@garfield.biz',
          body:
            'quia molestiae reprehenderit quasi aspernatur\naut expedita occaecati aliquam eveniet laudantium\nomnis quibusdam delectus saepe quia accusamus maiores nam est\ncum et ducimus et vero voluptates excepturi deleniti ratione'
        },
        {
          postId: 1,
          id: 4,
          name: 'alias odio sit',
          email: 'Lew@alysha.tv',
          body:
            'non et atque\noccaecati deserunt quas accusantium unde odit nobis qui voluptatem\nquia voluptas consequuntur itaque dolor\net qui rerum deleniti ut occaecati'
        },
        {
          postId: 1,
          id: 5,
          name: 'vero eaque aliquid doloribus et culpa',
          email: 'Hayden@althea.biz',
          body:
            'harum non quasi et ratione\ntempore iure ex voluptates in ratione\nharum architecto fugit inventore cupiditate\nvoluptates magni quo et'
        }
      ]
    };
  }

  inputChanged = (key, value) => {
    dataStore.request = update(dataStore.request, { [key]: { $set: value } });
  };

  inputRowChanged = (index, paramType, key, value) => {
    dataStore.request = update(dataStore.request, { [paramType]: { [index]: { [key]: { $set: value } } } });
  };

  addParam = paramType => {
    dataStore.request = update(dataStore.request, {
      [paramType]: {
        $push: [{ key: null, value: null }]
      }
    });
  };

  removeParam = (paramType, index) => {
    if (dataStore.request[paramType].length > 1) {
      dataStore.request = update(dataStore.request, {
        [paramType]: {
          $splice: [[index, 1]]
        }
      });
    } else {
      dataStore.request = update(dataStore.request, {
        [paramType]: {
          $set: [{ key: null, value: null }]
        }
      });
    }
  };

  showHistory = () => {
    uiStore.isLeftPanelOpen = !uiStore.isLeftPanelOpen;
    getRequests()
      .then(resp => {
        dataStore.history = resp.body.data;
      })
      .catch(err => console.log(err.error));
  };

  send = () => {
    let data = dataStore.request;
    this.setState({ isLoading: true, errors: {}, submitError: null });
    if (data.endpoint && data.method) {
      sendRequest(data)
        .then(res => {
          dataStore.result = res.body;
          this.setState({ isLoading: false });
          if (isLoggedIn()) {
            saveRequest(data)
              .then(resp => {})
              .catch(err => console.log(err.error));
            dataStore.history.push({ ...data, createdAt: new Date() });
          }
        })
        .catch(err => {
          console.log(err);
          dataStore.result = null;
          this.setState({ isLoading: false, submitError: 'Request error' });
        });
    } else {
      const errors = {};
      dataStore.result = null;
      if (!data.endpoint) errors.endpoint = 'Endpoint is required';
      if (!data.method) errors.method = 'Method is required';
      this.setState({ errors, isLoading: false });
    }
  };

  render() {
    return (
      <div className="requestSection">
        <div className="row">
          <a className="button" onClick={this.showHistory}>
            <span className="icon is-small">
              <i className="fas fa-history" />
            </span>
          </a>
          <DropDown
            questionKey="method"
            title="Select method"
            items={['POST', 'GET', 'DELETE', 'PUT']}
            inputChanged={this.inputChanged}
            value={dataStore.request.method}
            error={this.state.errors.method}
          />
          <div style={{ width: '100%' }}>
            <input
              className="endpointInput input"
              type="text"
              placeholder="Endpoint"
              onChange={e => this.inputChanged('endpoint', e.target.value)}
              value={dataStore.request.endpoint || ''}
            />
            {this.state.errors.endpoint && <p className="errorMessage">{this.state.errors.endpoint}</p>}
          </div>
          <div>
            <a className={`button ${this.state.isLoading ? 'is-loading' : ''}`} onClick={this.send}>
              Send
            </a>
            {this.state.submitError && <p className="errorMessage">{this.state.submitError}</p>}
          </div>
        </div>
        <div className="toggleButton">
          <button onClick={() => this.setState({ isParamSectionVisible: !this.state.isParamSectionVisible })}>
            Toggle parameters
          </button>
        </div>

        {this.state.isParamSectionVisible && (
          <div>
            <div className="columns">
              <div className="column">
                <p className="is-size-4 column-header">Header fields</p>

                {dataStore.request.requestHeaders.map((item, index) => (
                  <div className="row" key={index}>
                    <DropDown
                      questionKey="key"
                      title="Header field"
                      inputChanged={(key, value) => this.inputRowChanged(index, 'requestHeaders', key, value)}
                      value={item.key}
                      items={[
                        'Accept',
                        'Accept-Charset',
                        'Accept-Encoding',
                        'Accept-Language',
                        'Accept-Datetime',
                        'Authorization',
                        'Cookie',
                        'Content-Type',
                        'Content-Length',
                        'Content-MD5'
                      ]}
                    />
                    <p className="label centered">Value</p>
                    <input
                      className="input with-times"
                      type="text"
                      placeholder="Value"
                      onChange={e => this.inputRowChanged(index, 'requestHeaders', 'value', e.target.value)}
                      value={item.value || ''}
                    />

                    <a className="centered" onClick={() => this.removeParam('requestHeaders', index)}>
                      <span className="icon is-small">
                        <i className="fas fa-times" />
                      </span>
                    </a>

                    <a
                      className="button is-pulled-right add-button is-success"
                      style={
                        dataStore.request.requestHeaders.length != 1 &&
                        dataStore.request.requestHeaders.length != index + 1
                          ? { backgroundColor: 'white' }
                          : {}
                      }
                      onClick={() => this.addParam('requestHeaders')}
                    >
                      <span className="icon is-small">
                        <i className="fas fa-plus" />
                      </span>
                    </a>
                  </div>
                ))}
              </div>

              <div className="column">
                <p className="is-size-4 column-header">Query parameters</p>
                {dataStore.request.requestParameters.map((item, index) => (
                  <div className="row" key={index}>
                    <p className="label centered">Name</p>
                    <input
                      className="input"
                      type="text"
                      placeholder="Name"
                      onChange={e => this.inputRowChanged(index, 'requestParameters', 'key', e.target.value)}
                      value={item.key || ''}
                    />
                    <p className="label centered">Value</p>
                    <input
                      className="input with-times"
                      placeholder="Value"
                      onChange={e =>
                        this.inputRowChanged(index, 'requestParameters', 'value', e.target.value)
                      }
                      value={item.value || ''}
                    />
                    <a className="centered" onClick={() => this.removeParam('requestParameters', index)}>
                      <span className="icon is-small">
                        <i className="fas fa-times" />
                      </span>
                    </a>
                    <a
                      className="button is-pulled-right add-button is-success"
                      onClick={() => this.addParam('requestParameters')}
                      style={
                        dataStore.request.requestParameters.length != 1 &&
                        dataStore.request.requestParameters.length != index + 1
                          ? { backgroundColor: 'white' }
                          : {}
                      }
                    >
                      <span className="icon is-small">
                        <i className="fas fa-plus" />
                      </span>
                    </a>
                  </div>
                ))}
              </div>
            </div>
            {dataStore.request.method == 'POST' && (
              <div>
                <p className="label centered">Body</p>
                <textarea
                  className="textarea"
                  onChange={e => this.inputChanged('body', e.target.value)}
                  value={dataStore.request.body || ''}
                />
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default view(RequestSection);
