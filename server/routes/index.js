const express = require('express');
const usersRoutes = require('./users');
const requestRoutes = require('./requests');
const auth = require('./auth');
const jsondata = require('./jsondata');
const jwt = require('jsonwebtoken');

const apiRouter = express.Router();

apiRouter.use('/login', auth);
apiRouter.use('/users', usersRoutes);

apiRouter.use((req, res, next) => {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, 'secret', (err, decoded) => {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });
      }
      req.decoded = decoded;
      next();
    });
  } else {
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });
  }
});

apiRouter.use('/jsondata', jsondata);

apiRouter.use('/requests', requestRoutes);

module.exports = apiRouter;
