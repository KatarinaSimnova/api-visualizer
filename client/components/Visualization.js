import React, { Component } from 'react';
import { view } from 'react-easy-state';
import dataStore from 'stores/data-store';
import ReactJson from 'react-json-view';
import Table from './Table';
import { saveJson, isLoggedIn } from '../client';
import _ from 'lodash';

class Visualization extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: 0,
      isLoading: false,
      submitError: null,
      submitSuccess: null,
      dataPath: null
    };

    this.tabs = ['Raw Data', 'Table', 'Tree'];
  }

  getDataOnPath = () => {
    if (this.state.dataPath) {
      const newData = _.get(this.props.data, this.state.dataPath);
      if (newData) {
        return newData;
      }
    }
    return this.props.data;
  };

  getTabData = () => {
    switch (this.state.activeTab) {
      case 0:
        return this.checkSearched(JSON.stringify(this.props.data, null, 2));
      case 1:
        return (
          <div style={{ overflowX: 'scroll' }}>
            <div className="row data-path-input">
              <p className="label centered">Path:</p>
              <input
                className="input"
                type="text"
                onChange={e => this.setState({ dataPath: e.target.value })}
                value={this.state.dataPath || ''}
              />
            </div>
            <Table src={this.getDataOnPath()} />
          </div>
        );
      case 2:
        return this.props.isEditable ? (
          <ReactJson
            src={this.props.data}
            onEdit={({ updated_src }) => this.props.onChange(updated_src)}
            onAdd={({ updated_src }) => this.props.onChange(updated_src)}
            onDelete={({ updated_src }) => this.props.onChange(updated_src)}
          />
        ) : (
          <ReactJson src={this.props.data} />
        );
    }
  };

  checkSearched = text => {
    if (!text || !dataStore.searchedText) return <pre className="pre-code">{text}</pre>;
    const index = text.indexOf(dataStore.searchedText);
    if (index < 0) {
      return <pre className="pre-code">{text}</pre>;
    }
    return (
      <pre className="pre-code">
        {text.slice(0, index)}
        <span className="highlight">{dataStore.searchedText}</span>
        {text.slice(index + dataStore.searchedText.length, text.length)}
      </pre>
    );
  };

  exportData = () => {
    const data = JSON.stringify(this.props.data, null, 2);
  };

  saveJson = () => {
    this.setState({ isLoading: true, submitError: null, submitSuccess: null });
    dataStore.jsons.push({ ...this.props.data, createdAt: new Date() });
    const req = this.props.onUpdate ? this.props.onUpdate() : saveJson({ jsonData: this.props.data });

    req
      .then(res => {
        this.setState({ isLoading: false, submitSuccess: 'Success' });
      })
      .catch(err => {
        this.setState({ isLoading: false, submitError: 'Error' });
      });
  };

  render() {
    return (
      <div className="visualization-section">
        <div className="row">
          <div>
            <div className="tabs  is-toggle tab-nav">
              <ul>
                {this.tabs.map((item, i) => (
                  <li
                    key={i}
                    className={this.state.activeTab == i ? 'is-active' : ''}
                    onClick={() => this.setState({ activeTab: i })}
                  >
                    <a>{item}</a>
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div className="tool-bar">
            <input
              className="input search-input"
              type="text"
              placeholder="Search"
              onChange={e => dataStore.setSearched(e.target.value)}
            />
            {isLoggedIn() && [
              <a
                key={1}
                className={`button ${this.state.isLoading ? 'is-loading' : ''}`}
                onClick={this.saveJson}
              >
                Save
              </a>,
              this.state.submitSuccess && (
                <div key={2}>
                  <p className="successMessage">{this.state.submitSuccess}</p>
                </div>
              ),
              this.state.submitError && (
                <div key={3}>
                  <p className="errorMessage">{this.state.submitError}</p>
                </div>
              )
            ]}
          </div>
        </div>
        <div className="data-field">{this.props.data ? this.getTabData() : 'No data'}</div>
      </div>
    );
  }
}
export default view(Visualization);

/* <a key={0} className="button" onClick={this.exportData}>
                Export to JSON
              </a>, */
