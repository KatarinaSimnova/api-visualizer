import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { view } from 'react-easy-state';
import dataStore from 'stores/data-store';

class Table extends Component {
  getHeaders(src) {
    let headers = [];
    src.forEach(element => {
      if (typeof element == 'object' && !Array.isArray(element)) {
        headers = _.union(Object.keys(element), headers);
      } else {
        headers = _.union([null], headers);
      }
    });
    return headers;
  }
  checkSearched = text => {
    if (!text || !dataStore.searchedText) return text;
    const index = text.indexOf(dataStore.searchedText);
    if (index < 0) {
      return <span>{text}</span>;
    }
    return (
      <span>
        {text.slice(0, index)}
        <span className="highlight">{dataStore.searchedText}</span>
        {text.slice(index + dataStore.searchedText.length, text.length)}
      </span>
    );
  };

  render() {
    if (Array.isArray(this.props.src)) {
      const headers = this.getHeaders(this.props.src);
      return (
        <table border="1">
          {!(headers.length == 1 && headers[0] === null) && (
            <thead>
              <tr>{headers.map((h, i) => <th key={i}>{this.checkSearched(h) || ''}&nbsp;</th>)}</tr>
            </thead>
          )}
          <tbody>
            {this.props.src.map((element, i) => {
              if (typeof element != 'object' || Array.isArray(element)) {
                return (
                  <tr key={i}>
                    {headers.map(
                      (h, j) =>
                        h ? (
                          <td key={j}>&nbsp;</td>
                        ) : (
                          <td key={j}>
                            <Table src={element} />
                          </td>
                        )
                    )}
                  </tr>
                );
              }
              return (
                <tr key={i}>
                  {headers.map((h, j) => (
                    <td key={j}>
                      <Table src={element[h]} />
                    </td>
                  ))}
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    } else if (typeof this.props.src == 'object') {
      const objHeaders = Object.keys(this.props.src);
      return (
        <table border="1">
          <thead>
            <tr>{objHeaders.map((h, i) => <th key={i}>{this.checkSearched(h) || ''}</th>)}</tr>
          </thead>

          <tbody>
            <tr>
              {Object.keys(this.props.src).map((k, i) => (
                <td key={i}>
                  <Table src={this.props.src[k]} />
                </td>
              ))}
            </tr>
          </tbody>
        </table>
      );
    }
    return this.checkSearched(JSON.stringify(this.props.src || ''));
  }
}

Table.propTypes = {};

export default view(Table);
