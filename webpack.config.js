const path = require('path');
const webpack = require('webpack');
const FriendlyErrors = require('friendly-errors-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const chalk = require('chalk');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const port = process.env.PORT || 8000;
const isDev = process.env.NODE_ENV === 'development';

console.info(`Building...`);

const config = {
  entry: './client/index.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: isDev ? 'apivisualizer.bundle.js' : 'apivisualizer.bundle.[hash].js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.ejs$/,
        loader: 'ejs-loader'
      },
      {
        test: /\.scss/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: isDev
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: isDev,
              includePaths: [path.join(__dirname, 'assets')]
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[hash].[ext]'
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      '@nx-js/observer-util': '@nx-js/observer-util/dist/es.es6.js',
      'react-easy-state': 'react-easy-state/dist/es.es6.js',
      components: path.resolve(__dirname, './client/components'),
      constants: path.resolve(__dirname, './client/constants'),
      services: path.resolve(__dirname, './client/services'),
      utils: path.resolve(__dirname, './client/utils'),
      stores: path.resolve(__dirname, './client/stores'),
      intl: path.resolve(__dirname, './client/services/intl-service')
    }
  },
  devServer: {
    historyApiFallback: true,
    port,
    quiet: true,
    hot: true
  },
  performance: {
    hints: false
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'index.ejs',
      title: 'API-Visualizer',
      inject: true
    })
  ],
  devtool: 'source-map'
};

if (isDev) {
  config.plugins = [
    ...config.plugins,
    new webpack.NamedModulesPlugin(),
    new FriendlyErrors({
      compilationSuccessInfo: {
        messages: [`Application is running on ${chalk.bold.cyan(`http://localhost:${port}`)}`]
      }
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ];
} else {
  config.plugins = [
    ...config.plugins,
    new UglifyJSPlugin({
      sourceMap: process.env.NODE_ENV == 'production',
      parallel: true,
      cache: true,
      uglifyOptions: {
        mangle: true,
        ecma: 8,
        output: {
          comments: false
        }
      }
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new CopyWebpackPlugin([{ from: './static', to: './static' }]),
    new webpack.NoEmitOnErrorsPlugin(),
    new ProgressBarPlugin()
  ];
}

module.exports = config;
