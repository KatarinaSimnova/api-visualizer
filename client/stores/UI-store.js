import { store } from 'react-easy-state';

const state = store({
  isLeftPanelOpen: false
});

export default state;
