const Sequelize = require('sequelize');

const isDev = process.env.NODE_ENV === 'development';

module.exports = new Sequelize(
  process.env.DB_NAME || 'data-visualizer-db',
  process.env.DB_USER || 'root',
  isDev ? 'horse' : '743a2b307403c1d30445d06bd892f1ad555fa773b96d4fb5',
  {
    host: 'localhost',
    dialect: 'mysql'
  }
);
