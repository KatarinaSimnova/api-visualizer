const express = require('express');
const { UserModel } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const router = express.Router();

// create
router.post('/', (req, res) => {
  const { username, password } = req.body;
  // bcrypt.compare(password, hash, (err, res) => {
  UserModel.findOne({ where: { username } })
    .then(user => {
      bcrypt.compare(password, user.password, (err, reponse) => {
        if (reponse == true) {
          res.status(200).json({
            message: 'success',
            username,
            id: user.id,
            token: jwt.sign(
              {
                exp: Math.floor(Date.now() / 1000) + 60 * 60,
                data: req.body
              },
              'secret'
            )
          });
        } else {
          res.status(401).json({ error: 'Wrong name or password' });
        }
      });
    })
    .catch(() => {
      res.status(401).json({ error: 'Wrong name or password' });
    });
  // });
});

module.exports = router;
