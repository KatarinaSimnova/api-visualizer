require('dotenv').config();
const http = require('http');
const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const passport = require('passport');
const path = require('path');
const sequelize = require('./sequelize');
const apiRouter = require('./routes');
const consola = require('consola');
const cors = require('cors');

const app = express();

const handleUncaughtError = err => {
  consola.error(new Error(err));
};
process.on('uncaughtException', handleUncaughtError);
process.on('unhandledRejection', handleUncaughtError);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/public', express.static(path.join(__dirname, '../dist')));
app.use(cors());

// Passport
app.use(passport.initialize());

app.use('/api', apiRouter);

// sequelize.sync({ force: false }) deletes all tables, then recreates them (useful development)
sequelize
  .sync({ force: true })
  .then(() => {
    http.createServer(app).listen(9000);
  })
  .catch(err => {
    console.error('Unable to connect to database: 0', err);
  });
