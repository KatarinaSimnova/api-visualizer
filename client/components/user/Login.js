import React, { Component } from 'react';
import { login } from '../../client';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: null,
      password: null,
      errors: {},
      submitError: null,
      isLoading: false
    };
  }

  login = () => {
    this.setState({ isLoading: true, errors: {}, submitError: null });
    const errors = {};
    if (!this.state.name) {
      errors.name = 'Name is required';
    }
    if (!this.state.password) {
      errors.password = 'Password is required';
    }

    if (Object.keys(errors).length === 0) {
      login({ username: this.state.name, password: this.state.password })
        .then(() => {
          this.setState({ isLoading: false });
          this.props.history.goBack();
        })
        .catch(err => {
          this.setState({ isLoading: false, submitError: err.response.body.error || 'Server error' });
        });
      this.setState({ isLoading: false });
    } else {
      this.setState({ errors, isLoading: false });
    }
  };

  render() {
    return (
      <div>
        <p className="is-size-4 column-header">Log in</p>
        <div>
          <input
            className="input"
            type="text"
            placeholder="Name"
            onChange={e => this.setState({ name: e.target.value })}
            value={this.state.name || ''}
          />
          {this.state.errors.name && <p className="errorMessage">{this.state.errors.name}</p>}
        </div>
        <div>
          <input
            className="input"
            type="password"
            placeholder="Password"
            onChange={e => this.setState({ password: e.target.value })}
            value={this.state.password || ''}
          />
          {this.state.errors.password && <p className="errorMessage">{this.state.errors.password}</p>}
        </div>
        {this.state.submitError && <p className="errorMessage">{this.state.submitError}</p>}
        <a className={`button ${this.state.isLoading ? 'is-loading' : ''}`} onClick={this.login}>
          Log in
        </a>
      </div>
    );
  }
}
