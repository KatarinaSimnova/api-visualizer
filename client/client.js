import request from 'superagent';
import dataStore from './stores/data-store';
import uiStore from './stores/ui-store';

const port = 'http://207.154.223.212:9000/api/';

function newRequest(method, endpoint) {
  if (method == 'POST') {
    return request.post(endpoint);
  } else if (method == 'DELETE') {
    return request.delete(endpoint);
  } else if (method == 'PUT') {
    return request.put(endpoint);
  }

  return request.get(endpoint);
}

export function sendRequest(data) {
  const req = newRequest(data.method, data.endpoint);

  data.requestParameters.forEach(item => {
    if (item.key && item.value) {
      req.query({ [item.key]: item.value });
    }
  });

  data.requestHeaders.forEach(item => {
    if (item.key && item.value) {
      req.set(item.key, item.value);
    }
  });
  if (data.body) {
    req.send(JSON.parse(data.body));
  }
  return req;
}

export function getRequests() {
  let req = newRequest('GET', `${port}requests`)
    .set('x-access-token', localStorage.getItem('token'))
    .query({ userId: localStorage.getItem('userId') });
  return req;
}

export function saveRequest(data) {
  const req = newRequest('POST', `${port}requests`)
    .set('x-access-token', localStorage.getItem('token'))
    .send({ ...data, userId: dataStore.user.id }); // .then(() => getRequests());
  return req;
}

export function signup(data) {
  return newRequest('POST', `${port}users`)
    .send(data)
    .then(res => {
      localStorage.setItem('token', res.body.token);
      localStorage.setItem('userId', res.body.id);
      dataStore.user = res.body;
      uiStore.isLeftPanelOpen = false;
      return res;
    });
}

export function login(data) {
  return newRequest('POST', `${port}login`)
    .send(data)
    .then(res => {
      localStorage.setItem('token', res.body.token);
      localStorage.setItem('userId', res.body.id);
      dataStore.user = res.body;
      uiStore.isLeftPanelOpen = false;
      return res;
    });
}

export function logout() {
  localStorage.removeItem('token');
  localStorage.removeItem('userId');
  dataStore.clearState();
  uiStore.isLeftPanelOpen = false;
}

export function isLoggedIn() {
  return !!localStorage.getItem('token');
}

export function saveJson(data) {
  let req = newRequest('POST', `${port}jsondata`)
    .set('x-access-token', localStorage.getItem('token'))
    .send({ ...data, userId: dataStore.user.id });
  return req;
}

export function getJsons() {
  const userid = localStorage.getItem('userId');
  let req = newRequest('GET', `${port}jsondata`)
    .query({ userId: userid })
    .set('x-access-token', localStorage.getItem('token'))

    .then(res => {
      console.log(res.body);
      dataStore.jsons = res.body.data;
    })
    .catch(err => console.log(err));
  return req;
}

export function deleteJson(id) {
  const req = newRequest('DELETE', `${port}jsondata/${id}`)
    .set('x-access-token', localStorage.getItem('token'))
    .then(res => {
      console.log(res.body);
      getJsons();
    })
    .catch(err => console.log(err));
  return req;
}

export function updateJson(id, data) {
  const req = newRequest('PUT', `${port}jsondata/${id}`)
    .send(data)
    .set('x-access-token', localStorage.getItem('token'))
    .then(res => {
      console.log(res.body);
      getJsons();
    })
    .catch(err => console.log(err));
  return req;
}

export function fetchUser() {
  return newRequest('GET', `${port}users/${localStorage.getItem('userId')}`)
    .then(res => {
      dataStore.user = res.body.data;
    })
    .catch(error => {
      console.log(error);
    });
}
