import React, { Component } from 'react';
import { withRouter, HashRouter as Router, Route, Link } from 'react-router-dom';
import { view } from 'react-easy-state';
import Header from './Header';
import Main from './Main';
import Auth from './user/Auth';
import MyData from './MyData';
import dataStore from '../stores/data-store';
import { fetchUser, isLoggedIn } from '../client';

class App extends Component {
  render() {
    if (!dataStore.user && localStorage.getItem('userId')) {
      fetchUser();
    }
    return (
      <Router>
        <div>
          <Header />
          <Route exact path="/" component={Main} />
          <Route path="/login" component={Auth} />
          <Route path="/mydata" render={props => (isLoggedIn() ? <MyData {...props} /> : null)} />
        </div>
      </Router>
    );
  }
}

export default view(App);
