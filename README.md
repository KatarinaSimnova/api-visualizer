# Client

## Setup
Install dependencies
```sh
$ npm install
```

## Development
Run the local webpack-dev-server with livereload and autocompile on [http://localhost:8000/](http://localhost:8000/)
```sh
$ npm start
```

# Server

* server IP: 159.89.29.254 ("ssh root@159.89.29.254")

## Dev setup
* install mysql database
```sh
$ choco install mysql
```
* create database with name "data-visualizer-db"
```sh
$ mysqladmin -u root -p create data-visualizer-db
```

```sh
$ yarn run server:dev
```

## Deploying
ssh root@159.89.29.254
cd node-app
git pull
