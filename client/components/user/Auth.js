import React, { Component } from 'react';
import Login from './Login';
import SignUp from './SignUp';

export default class Auth extends Component {
  render() {
    return (
      <div className="columns">
        <div className="column login-column">
          <Login history={this.props.history} />
        </div>
        <div className="column login-column">
          <SignUp history={this.props.history} />
        </div>
      </div>
    );
  }
}
