import { store } from 'react-easy-state';

const state = store({
  request: {
    endpoint: 'https://jsonplaceholder.typicode.com/comments',
    method: 'GET',
    body: null,
    requestHeaders: [{ key: 'content-type', value: 'application/json' }],
    requestParameters: [{ key: 'postId', value: 1 }]
  },
  updateRequest(data) {
    this.request = Object.assign(
      { requestHeaders: [{ key: 'content-type', value: 'application/json' }], requestParameters: [{}] },
      data
    );
  },
  result: null,
  searchedText: null,
  setSearched(text) {
    this.searchedText = text;
  },
  history: [],
  addRequestToHistory(data) {
    this.history.push(data);
  },
  user: null,
  clearState() {
    this.result = null;
    this.searchedText = null;
    this.history = [];
    this.request = {
      requestHeaders: [{ key: 'content-type', value: 'application/json' }],
      requestParameters: [{}]
    };
    this.user = null;
  },
  jsons: []
});

export default state;
