import React, { Component } from 'react';
import { signup } from '../../client';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: null,
      password: null,
      passwordCheck: null,
      submitError: null,
      errors: {},
      isLoading: false
    };
  }

  signup = () => {
    this.setState({ isLoading: true, errors: {}, submitError: null });
    const errors = {};
    if (!this.state.name) {
      errors.name = 'Name is required';
    }
    if (!this.state.password) {
      errors.password = 'Password is required';
    }

    if (!this.state.passwordCheck) {
      errors.passwordCheck = 'Password check is required';
    }

    if (Object.keys(errors).length === 0) {
      if (this.state.password == this.state.passwordCheck) {
        if (this.state.password.length > 5) {
          signup({ username: this.state.name, password: this.state.password })
            .then(() => {
              this.setState({ isLoading: false });
              this.props.history.goBack();
            })
            .catch(error => {
              this.setState({ isLoading: false, submitError: error.response.body.error || 'Server error' });
            });
        } else {
          this.setState({ submitError: 'Password can not be less than 6 characters', isLoading: false });
        }
      } else {
        this.setState({ submitError: "Passwords dont't match", isLoading: false });
      }
    } else {
      this.setState({ errors, isLoading: false });
    }
  };

  render() {
    return (
      <div>
        <p className="is-size-4 column-header">Sign up</p>
        <div>
          <input
            className="input"
            type="text"
            placeholder="Name"
            onChange={e => this.setState({ name: e.target.value })}
            value={this.state.name || ''}
          />
          {this.state.errors.name && <p className="errorMessage">{this.state.errors.name}</p>}
        </div>
        <div>
          <input
            className="input"
            type="password"
            placeholder="Password"
            onChange={e => this.setState({ password: e.target.value })}
            value={this.state.password || ''}
          />
          {this.state.errors.password && <p className="errorMessage">{this.state.errors.password}</p>}
        </div>
        <div>
          <input
            className="input"
            type="password"
            placeholder="Password check"
            onChange={e => this.setState({ passwordCheck: e.target.value })}
            value={this.state.passwordCheck || ''}
          />
          {this.state.errors.passwordCheck && (
            <p className="errorMessage">{this.state.errors.passwordCheck}</p>
          )}
        </div>
        {this.state.submitError && <p className="errorMessage">{this.state.submitError}</p>}
        <a className={`button ${this.state.isLoading ? 'is-loading' : ''}`} onClick={this.signup}>
          Sign up
        </a>
      </div>
    );
  }
}
