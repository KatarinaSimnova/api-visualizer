const sequelize = require('./sequelize');
const { STRING } = require('sequelize');
const DataTypes = require('sequelize/lib/data-types');

const UserModel = sequelize.define('user', {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV1,
    primaryKey: true
  },
  username: {
    type: STRING,
    unique: true,
    allowNull: false
  },
  password: {
    type: STRING,
    allowNull: false
  }
});

const RequestModel = sequelize.define('request', {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV1,
    primaryKey: true
  },
  method: {
    type: STRING,
    allowNull: false
  },
  endpoint: {
    type: STRING,
    allowNull: false
  }
});

const RequestParameterModel = sequelize.define('requestParameter', {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV1,
    primaryKey: true
  },
  key: {
    type: STRING,
    allowNull: false
  },
  value: {
    type: STRING,
    allowNull: false
  }
});

const RequestHeaderModel = sequelize.define('requestHeader', {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV1,
    primaryKey: true
  },
  key: {
    type: STRING,
    allowNull: false
  },
  value: {
    type: STRING,
    allowNull: false
  }
});

const JsonDataModel = sequelize.define('jsonData', {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV1,
    primaryKey: true
  },
  fileId: {
    type: DataTypes.STRING,
    defaultValue: ''
  }
});

UserModel.hasMany(RequestModel, { as: 'requests', foreignKey: { allowNull: false }, onDelete: 'CASCADE' });
RequestModel.belongsTo(UserModel, {
  allowNull: false,
  foreignKey: { allowNull: false },
  onDelete: 'CASCADE'
});

UserModel.hasMany(JsonDataModel, { as: 'jsonData', foreignKey: { allowNull: false }, onDelete: 'CASCADE' });
JsonDataModel.belongsTo(UserModel, {
  allowNull: false,
  foreignKey: { allowNull: false },
  onDelete: 'CASCADE'
});

RequestModel.hasMany(RequestParameterModel, {
  as: 'requestParameters',
  foreignKey: { allowNull: false },
  onDelete: 'CASCADE'
});

RequestParameterModel.belongsTo(RequestModel, {
  allowNull: false,
  foreignKey: { allowNull: false },
  onDelete: 'CASCADE'
});

RequestModel.hasMany(RequestHeaderModel, {
  as: 'requestHeaders',
  foreignKey: { allowNull: false },
  onDelete: 'CASCADE'
});

RequestHeaderModel.belongsTo(RequestModel, {
  allowNull: false,
  foreignKey: { allowNull: false },
  onDelete: 'CASCADE'
});

module.exports = { UserModel, RequestModel, RequestParameterModel, RequestHeaderModel, JsonDataModel };
