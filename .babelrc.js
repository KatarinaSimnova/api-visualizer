const config = {
  presets: [
    ['@babel/preset-react', { development: process.env.NODE_ENV === "development" }],
    '@babel/preset-typescript'
  ],
  plugins: [
    ['direct-import', {
      modules: [
          'react-router',
          'react-router-dom'
        ]
      }
    ],
    ['@babel/proposal-class-properties', { loose: true }],
    ['@babel/proposal-decorators', { loose: true, legacy: true }],
    ['@babel/proposal-object-rest-spread', { loose: true }]
  ],
  ignore: ['node_modules', 'build']
}

if (process.env.NODE_ENV == 'production') {
  config.presets = config.presets.concat([
    ['@babel/preset-env', {
      targets: { browsers: [ 'edge > 13' ] },
      useBuiltIns: 'usage',
      modules: false,
      loose: true
    }]
  ]),
  config.plugins.concat([
    '@babel/transform-react-inline-elements',
    '@babel/transform-react-constant-elements'
  ])
}

module.exports = config;
