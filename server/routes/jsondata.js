const express = require('express');
const { JsonDataModel } = require('../models');
const { isEntityNotFoundWhileUpdating } = require('../utils');
const getUUID = require('uuid/v1');
const { join } = require('path');
const { writeJson, readJson, remove } = require('fs-extra');

const router = express.Router();

// create
router.post('/', (req, res) => {
  const data = req.body;
  const fileId = getUUID();

  writeJson(join(__dirname, `../requests/${fileId}.json`), data.jsonData)
    .then(() => {
      JsonDataModel.create(Object.assign(data, { fileId })).then(() => {
        res.status(201).json({ message: 'success' });
      });
    })
    .catch(error => {
      res.status(422).json({ error });
    });
});

// update
router.put('/:id', (req, res) => {
  const data = req.body;
  JsonDataModel.findById(req.params.id, { include: [{ all: true }] })
    .then(result => writeJson(join(__dirname, `../requests/${result.fileId}.json`), data.jsonData))
    .then(() => {
      const { jsonData, id, fileId, ...dataToUpdate } = data;
      JsonDataModel.update(dataToUpdate, { where: { id: req.params.id } })
        .then(result => {
          if (isEntityNotFoundWhileUpdating(result)) throw 'Not found';
          res.status(202).json({ message: 'success', id: result.id });
        })
        .catch(error => {
          res.status(422).json({ error });
        });
    });
});

// delete
router.delete('/:id', (req, res) => {
  JsonDataModel.findById(req.params.id, { include: [{ all: true }] })
    .then(result => {
      remove(join(__dirname, `../requests/${result.fileId}.json`));
    })
    .then(() => {
      JsonDataModel.destroy({ where: { id: req.params.id } }).then(result => {
        res.status(200).json({ message: 'success', id: result.id });
      });
    })
    .catch(error => {
      res.status(422).json({ error });
    });
});

// get all
router.get('/', (req, res) => {
  const resultMap = [];
  JsonDataModel.findAll({ where: { userId: req.query.userId } })
    .then(result => {
      const fileIds = result.map(data => data.fileId);

      return Promise.all(
        fileIds.map(fileId =>
          readJson(join(__dirname, `../requests/${fileId}.json`)).then(data => ({
            data,
            fileId
          }))
        )
      ).then(resultsFromFile => {
        result.forEach(jsonData => {
          const dataFromFile = resultsFromFile.find(row => row.fileId === jsonData.fileId).data;
          const data = JSON.parse(JSON.stringify(jsonData));
          resultMap.push(Object.assign(data, { jsonData: dataFromFile }));
        });
      });
    })
    .then(() => {
      res.status(200).json({ message: 'success', data: resultMap });
    })
    .catch(error => {
      res.status(404).json({ error });
    });
});

// // get all
// router.get('/', (req, res) => {
//   const resultMap = [];
//   JsonDataModel.findAll({ include: [{ all: true }] })
//     .then(result => {
//       const fileIds = result.map(data => data.fileId);

//       return Promise.all(
//         fileIds.map(fileId =>
//           readJson(join(__dirname, `../requests/${fileId}.json`)).then(data => ({
//             data: JSON.parse(data),
//             fileId
//           }))
//         )
//       ).then(resultsFromFile => {
//         result.forEach(jsonData => {
//           const dataFromFile = resultsFromFile.find(row => row.fileId === jsonData.fileId).data;
//           const data = JSON.parse(JSON.stringify(jsonData));
//           resultMap.push(Object.assign(data, { jsonData: dataFromFile }));
//         });
//       });
//     })
//     .then(() => {
//       res.status(200).json({ message: 'success', data: resultMap });
//     })
//     .catch(error => {
//       res.status(404).json({ error });
//     });
// });

module.exports = router;
