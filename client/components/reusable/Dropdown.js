import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class DropDown extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };
  }

  componentDidMount = () => {
    window.addEventListener('click', this.handleWindowClick);
  };

  componentWillUnmount = () => {
    window.removeEventListener('click', this.handleWindowClick);
  };

  handleWindowClick = e => {
    if (e.target.className && typeof e.target.className == 'string') {
      if (e.target.className != 'dropdown-title' && !e.target.className.includes('dropdown')) {
        this.setState({ isOpen: false });
      }
    } else if (e.target.class && typeof e.target.class == 'string') {
      if (!e.target.class.includes('dropdown')) {
        this.setState({ isOpen: false });
      }
    }
  };

  itemPicked = item => {
    this.setState({ isOpen: false });
    this.props.inputChanged(this.props.questionKey, item);
  };

  render() {
    return (
      <div>
        <div className={`dropdown ${this.state.isOpen && 'is-active'}`}>
          <div className="dropdown-trigger">
            <button
              className="button dropdown-button"
              aria-haspopup="true"
              aria-controls="dropdown-menu"
              onClick={() => this.setState({ isOpen: !this.state.isOpen })}
            >
              <span className="dropdown-title">{this.props.value || this.props.title}</span>
              <span className="icon is-small dropdown-icon">
                <i className="fas fa-angle-down dropdown-icon" aria-hidden="true" />
              </span>
            </button>
          </div>
          <div className="dropdown-menu" id="dropdown-menu" role="menu">
            <div className="dropdown-content">
              {this.props.items.map((item, i) => (
                <a
                  key={i}
                  className={`dropdown-item ${this.props.value == item && 'is-active'}`}
                  onClick={() => this.itemPicked(item)}
                >
                  {item}
                </a>
              ))}
            </div>
          </div>
        </div>
        {this.props.error && <p className="errorMessage">{this.props.error}</p>}
      </div>
    );
  }
}

DropDown.propTypes = {
  title: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
  questionKey: PropTypes.string.isRequired
};
