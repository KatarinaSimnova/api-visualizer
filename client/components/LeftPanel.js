import React, { Component } from 'react';
import uiStore from 'stores/ui-store';
import dataStore from 'stores/data-store';
import { view } from 'react-easy-state';
import Tooltip from 'react-simple-tooltip';
import { isLoggedIn } from '../client';

class LeftPanel extends Component {
  render() {
    return (
      <div className="left-panel">
        <div className="row">
          <h1 className="is-size-5">History</h1>
          <a
            onClick={() => {
              uiStore.isLeftPanelOpen = false;
            }}
            className="is-pulled-right borderless-button"
          >
            <span className="icon is-small">
              <i className="fas fa-times" />
            </span>
          </a>
        </div>
        {dataStore.history.map((h, i) => (
          <a key={i} className="borderless-button" onClick={() => dataStore.updateRequest(h)}>
            <div className="history-item">
              <p>{new Date(h.createdAt).toLocaleString('de')}</p>
              <Tooltip
                content={h.endpoint}
                className="tooltip"
                border="#dbdbdb"
                background="#eee"
                color="#000"
                placement="right"
                padding={5}
              >
                <p className="history-title">{h.endpoint}</p>
              </Tooltip>
            </div>
          </a>
        ))}
        {!isLoggedIn() && <p>You need to Log in.</p>}
      </div>
    );
  }
}

export default view(LeftPanel);
