import React, { Component } from 'react';
import { view } from 'react-easy-state';
import RequestSection from './RequestSection';
import Visualization from './Visualization';
import LeftPanel from './LeftPanel';
import uiStore from 'stores/ui-store';
import dataStore from 'stores/data-store';

class Main extends Component {
  render() {
    return (
      <div className="mainWindow">
        {uiStore.isLeftPanelOpen && <LeftPanel />}
        <div className="main-content">
          <RequestSection />
          <Visualization data={dataStore.result} />
        </div>
      </div>
    );
  }
}

export default view(Main);
